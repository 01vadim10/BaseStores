﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;

namespace BaseStores.Domain.Entities
{
    public class Store
    {
        [HiddenInput(DisplayValue = false)]
        public int StoreID { get; set; }

        [Required(ErrorMessage = "Please enter a Store name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a Address Store")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter a Mode work Store")]
        public string Mode { get; set; }
    }
}
