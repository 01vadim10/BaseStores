﻿using System.Linq;
using BaseStores.Domain.Entities;

namespace BaseStores.Domain.Abstract
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
        void SaveProduct(Product product);
        Product DeleteProduct(int productId);
    }
}
