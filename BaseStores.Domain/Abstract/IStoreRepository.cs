﻿using System.Linq;
using BaseStores.Domain.Entities;   

namespace BaseStores.Domain.Abstract
{
    public interface IStoreRepository
    {
        IQueryable<Store> Stores { get; }

        void SaveStore(Store store);
        
        Store DeleteStore(int storeId);
    }
}
