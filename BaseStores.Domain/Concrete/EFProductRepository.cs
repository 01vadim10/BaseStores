﻿using System.Linq;
using BaseStores.Domain.Abstract;
using BaseStores.Domain.Entities;

namespace BaseStores.Domain.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private readonly EFDbContext _context = new EFDbContext();
        public IQueryable<Product> Products
        {
            get { return _context.Products; }
        }

        public void SaveProduct(Product product)
        {
            if (product.ProductID == 0)
            {
                _context.Products.Add(product);
            }
            else
            {
                var dbEntry = _context.Products.Find(product.ProductID);
                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.StoreID = product.StoreID;
                }
            }
            _context.SaveChanges();
        }

        public Product DeleteProduct(int productId)
        {
            var dbEntry = _context.Products.Find(productId);
            if (dbEntry == null) return null;
            _context.Products.Remove(dbEntry);
            _context.SaveChanges();
            return dbEntry;
        }
    }
}
