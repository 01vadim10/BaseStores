﻿using BaseStores.Domain.Entities;
using System.Data.Entity;

namespace BaseStores.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Store> Stores { get; set; }
    }
}
