﻿using System.Linq;
using BaseStores.Domain.Abstract;
using BaseStores.Domain.Entities;

namespace BaseStores.Domain.Concrete
{
    public class EFStoreRepository : IStoreRepository
    {
        private readonly EFDbContext _context = new EFDbContext();
        public IQueryable<Store> Stores
        {
            get { return _context.Stores; }
        }

        public void SaveStore(Store store)
        {
            if (store.StoreID == 0)
            {
                _context.Stores.Add(store);
            }
            else
            {
                var dbEntry = _context.Stores.Find(store.StoreID);
                if (dbEntry != null)
                {
                    dbEntry.Name = store.Name;
                    dbEntry.Address = store.Address;
                    dbEntry.Mode = store.Mode;
                }
            }
            _context.SaveChanges();
        }

        public Store DeleteStore(int storeId)
        {
            var dbEntry = _context.Stores.Find(storeId);
            if (dbEntry == null) return null;
            _context.Stores.Remove(dbEntry);
            _context.SaveChanges();
            return dbEntry;
        }
    }
}
