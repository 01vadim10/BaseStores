﻿CREATE TABLE [dbo].[Stores] (
    [StoreID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (100) NOT NULL,
    [Address]  NVARCHAR (100) NOT NULL,
    [Mode]    NVARCHAR (50)  NOT NULL,
    PRIMARY KEY CLUSTERED ([StoreID] ASC)
);

