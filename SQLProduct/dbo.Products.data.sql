﻿SET IDENTITY_INSERT [dbo].[Products] ON
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (1, N'Ice-cream', N'Many many words', 1)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (2, N'Bread', N'Many many words', 1)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (3, N'Candies', N'Many many words', 1)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (4, N'AtMega16', N'Many many words', 3)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (5, N'ATMega8', N'Many many words', 3)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (6, N'Rashguard', N'Many many words', 4)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (7, N'Shorts', N'Many many words', 4)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (8, N'sadfasdf', N'sadfsadf', 5)
INSERT INTO [dbo].[Products] ([ProductID], [Name], [Description], [StoreID]) VALUES (9, N'gdfgdfg', N'fggg', 9)
SET IDENTITY_INSERT [dbo].[Products] OFF
