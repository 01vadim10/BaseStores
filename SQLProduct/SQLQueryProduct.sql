﻿CREATE TABLE [dbo].[Products] (
    [ProductID]   INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [Description] NVARCHAR (500) NOT NULL,
    [StoreID]     INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [FK_Products_FromStores] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Stores] ([StoreID])
);

