﻿using System.Collections.Generic;
using BaseStores.Domain.Entities;

namespace BaseStores.Models
{
    public class ProductsListViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public string CurrentStoreName { get; set; }
        public int CurrentStoreId { get; set; }
    }
}