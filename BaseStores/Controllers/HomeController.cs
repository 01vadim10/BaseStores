﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BaseStores.Domain.Abstract;
using BaseStores.Domain.Entities;
using BaseStores.Models;
using Ninject.Infrastructure.Language;

namespace BaseStores.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly IStoreRepository _storeRepository;
        const int pageSize = 30;

        public HomeController(IProductRepository productRepo, IStoreRepository storeRepo)
        {
            _productRepository = productRepo;
            _storeRepository = storeRepo;
        }

        public ActionResult StoresList(int? id)
        {
            int page = id ?? 0;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Stores", GetStoresPage(page));
            }
            return View(GetStoresPage(page));
        }

        public IEnumerable<Store> GetStoresPage(int page = 1)
        {
            var itemsToSkip = page * pageSize;

            return _storeRepository.Stores.OrderBy(t => t.StoreID)
                    .Skip(itemsToSkip).Take(pageSize).ToEnumerable();
        }

        public ViewResult ProductsList(Store item)
        {
            var viewModel = new ProductsListViewModel
            {
                Products = (from products in _productRepository.Products
                    where products.StoreID == item.StoreID
                    select products),
                CurrentStoreName = item.Name,
                CurrentStoreId = item.StoreID
            };

            return View(viewModel);
        }

        public ViewResult EditProduct(int productId)
        {
            var product = _productRepository.Products
                                .FirstOrDefault(p => p.ProductID == productId);
            return View(product);
        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                _productRepository.SaveProduct(product);
                TempData["message"] = string.Format("{0} has been saved", product.Name);
                return RedirectToAction("ProductsList", new { product.StoreID });
            }
            else
            {
                TempData["message"] = string.Format("{0} has not been saved", product.Name);
                return RedirectToAction("ProductsList", new { product.StoreID });
            }
        }

        public ViewResult CreateProduct(int currentStoreId)
        {
            return View("EditProduct", new Product { StoreID = currentStoreId });
        }

        [HttpPost]
        public ActionResult DeleteProduct(int productId, int storeId)
        {
            var deletedProduct = _productRepository.DeleteProduct(productId);
            if (deletedProduct != null)
            {
                TempData["message"] = string.Format("Товар {0} был удален", deletedProduct.Name);
            }
            return RedirectToAction("ProductsList", new { storeId });
        }

        public ViewResult EditStore(int storeId)
        {
            var store = _storeRepository.Stores
                                .FirstOrDefault(p => p.StoreID == storeId);
            return View(store);
        }

        [HttpPost]
        public ActionResult EditStore (Store store)
        {
            if (ModelState.IsValid)
            {
                _storeRepository.SaveStore(store);
                TempData["message"] = string.Format("{0} has been saved", store.Name);
                return RedirectToAction("StoresList");
            }
            else
            {
                TempData["message"] = string.Format("{0} has not been saved", store.Name);
                return View();
            }
        }

        public ViewResult CreateStore()
        {
            return View("EditStore", new Store());
        }

        [HttpPost]
        public ActionResult DeleteStore(int storeId)
        {
            var deletedStore = _storeRepository.DeleteStore(storeId);
            if (deletedStore != null)
            {
                TempData["message"] = string.Format("Магазин {0} был удален", deletedStore.Name);
            }
            return RedirectToAction("StoresList");
        }
    }
}
